const apiKey = "1a8a741d02a03faee802f8d0a9c19884";

const searchInput = document.querySelector(".search-input");
const cityName = document.querySelector(".city");
const temp = document.querySelector(".temperature");
const hum = document.querySelector(".humidity");
const description = document.querySelector(".description");
const wind = document.querySelector(".wind-speed");
const background = document.body
const containerBackground = document.querySelector(".header");
const container = document.querySelector(".container");
const icon = document.querySelector(".icon-img");
const iconClass = document.querySelector(".icon");

searchInput.addEventListener("keyup", enterPressed);

function enterPressed (event) {

    if(event.key === "Enter") {

        findWeatherDetails();

    }

}

function findWeatherDetails () {

    if (searchInput.value === "") {}

    else {

        const searchLink = `https://api.openweathermap.org/data/2.5/weather?q=${searchInput.value}&appid=${apiKey}`;
        
        httpRequestAsync(searchLink, theResponse);

    }

}

function httpRequestAsync(url, callback) {

    const httpRequest = new XMLHttpRequest();

    httpRequest.onreadystatechange = () => {

        if (httpRequest.readyState == 4 && httpRequest.status == 200) {

            callback(httpRequest.responseText);

        }

    }
    httpRequest.open("Get", url, true);
    httpRequest.send();

}

function theResponse (response) {

    const jsonObject = JSON.parse(response);

    const iconResponse = jsonObject.weather[0].icon;
    cityName.innerHTML = jsonObject.name;
    temp.innerHTML = parseInt(jsonObject.main.temp - 273) + "°";
    hum.innerHTML = jsonObject.main.humidity + "%";
    description.innerHTML = jsonObject.weather[0].description;
    wind.innerHTML = parseInt(jsonObject.wind.speed * 3.6) + "km/h";
    icon.setAttribute("src", `http://openweathermap.org/img/wn/${iconResponse}@2x.png`);

    if (parseInt(jsonObject.main.temp - 273) <= 15) {

        background.classList.add("winter-background");
        background.classList.remove("hot-background");
        container.classList.add("font-change-blue");

    }

    else if (parseInt(jsonObject.main.temp - 273) >= 30) {

        background.classList.add("hot-background");
        background.classList.remove("winter-background");
        container.classList.remove("font-change-blue");


    }

    else {

        background.classList.remove("hot-background");
        background.classList.remove("winter-background");
        container.classList.remove("font-change-blue")

    }

}

iconClass.addEventListener("click", () => {

    icon.classList.add(".shake");

    setTimeout(() => {

        icon.classList.remove(".shake");

    }, 500);

})